from detectors import YOLO
import cv2
from PIL import Image
from tracker_center import Tracker
import numpy as np
from keras import backend as K
import time
import matplotlib.pyplot as plt

ix, iy, ex, ey = -1, -1, -1, -1
cap_from_stream = True
path = 'coconut1.m4v'
# path = 'rtsp://192.168.10.16:554'
fixed_size = (480, 853)


def limit_mem():
    K.get_session().close()
    cfg = K.tf.ConfigProto()
    cfg.gpu_options.allow_growth = True
    K.set_session(K.tf.Session(config=cfg))


def draw_rec(event, x, y, flags, param):
    global ix, iy, ex, ey, drawing, mode

    if event == cv2.EVENT_LBUTTONDOWN:
        ix, iy = x, y

    elif event == cv2.EVENT_LBUTTONUP:
        ex, ey = x, y
        cv2.rectangle(param, (ix, iy), (x, y), (0, 255, 0), 0)


def get_crop_size(video_path):
    cap = cv2.VideoCapture(video_path)
    while cap.isOpened():
        ret, frame = cap.read()
        if cap_from_stream:
            frame = cv2.resize(frame, fixed_size)
        cv2.namedWindow('draw_rectangle')
        cv2.setMouseCallback('draw_rectangle', draw_rec, frame)
        print("Choose your area of interest!")
        while 1:
            cv2.imshow('draw_rectangle', frame)
            k = cv2.waitKey(1) & 0xFF
            if k == ord('a'):
                cv2.destroyAllWindows()
                break
        break


def main():
    limit_mem()
    # Choose area of interest
    get_crop_size(path)
    print('Your area of interest: ', ix, ' ', iy, ' ', ex, ' ', ey)
    area = (ix, iy, ex, ey)

    # Create opencv video capture object
    cap = cv2.VideoCapture(path)
    w = int(cap.get(3))
    h = int(cap.get(4))
    if cap_from_stream:
        w = fixed_size[0]
        h = fixed_size[1]
    video_fps = cap.get(cv2.CAP_PROP_FPS)
    fourcc = cv2.VideoWriter_fourcc(*'MJPG')
    out = cv2.VideoWriter('res.avi', fourcc, video_fps, (w + h, h))

    # Create Object Detector
    detector = YOLO()

    # Create Object Tracker
    tracker = Tracker(dist_thresh=40.0, max_frames_to_skip=5, max_trace_length=10, trackIdCount=0)

    # Variables initialization
    track_colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0),
                    (0, 255, 255), (255, 0, 255), (255, 127, 255),
                    (127, 0, 255), (127, 0, 127)]

    count_vehicle = {'coconut': 0}

    number_of_skipped_frame = 1000
    cnt = 0

    time_counter = 0
    frame_counter = 0
    xar = []
    yar = []
    fig = plt.figure(figsize=(6, 6))
    ax = fig.add_subplot(1, 1, 1)
    count_local = 0

    while cap.isOpened():
        # Capture frame-by-frame
        ret, frame = cap.read()
        cnt += 1
        if cnt <= number_of_skipped_frame:
            start = time.time()
            continue
        if cap_from_stream:
            frame = cv2.resize(frame, fixed_size)
        frame = Image.fromarray(frame)
        frame_counter += 1
        # Detect and return centeroids of the objects in the frame
        result, centers, box_detected, obj_type, _ = detector.detect_image(frame, area)
        result = np.asarray(result)

        output = result.copy()
        alpha = 0.3
        for i in range(len(box_detected)):
            left, top, right, bottom = box_detected[i][0], box_detected[i][1], box_detected[i][2], box_detected[i][3]
            cv2.rectangle(result, (left, top), (right, bottom), (0, 255, 0), -1)
            cv2.addWeighted(result, alpha, output, 1 - alpha, 0, output)

        result = output.copy()

        print('Number of detections: ', len(centers))

        # If centroids are detected then track them
        if len(box_detected) > 0:

            # Track object using Kalman Filter
            tracker.Update(box_detected, obj_type)

            # For identified object tracks draw tracking line
            # Use various colors to indicate different track_id
            for i in range(len(tracker.tracks)):
                if len(tracker.tracks[i].trace) >= 2:
                    for j in range(len(tracker.tracks[i].trace) - 1):
                        # Draw trace line
                        x1 = tracker.tracks[i].trace[j][0][0]
                        y1 = tracker.tracks[i].trace[j][1][0]
                        x2 = tracker.tracks[i].trace[j + 1][0][0]
                        y2 = tracker.tracks[i].trace[j + 1][1][0]
                        clr = tracker.tracks[i].track_id % 9
                        cv2.line(result, (int(x1), int(y1)), (int(x2), int(y2)),
                                 track_colors[clr], 2)
                    classes = tracker.tracks[i].get_obj()
                    if (len(tracker.tracks[i].trace) >= 10) and (not tracker.tracks[i].counted):
                        tracker.tracks[i].counted = True
                        count_vehicle[classes] += 1
                        count_local += 1
                        if tracker.tracks[i].ground_truth_box is not None:
                            bbox = tracker.tracks[i].ground_truth_box.reshape((4, 1))
                            cv2.rectangle(result, (bbox[0][0], bbox[1][0]), (bbox[2][0], bbox[3][0]),
                                          color=(255, 0, 255), thickness=3)

        # Draw chart
        if frame_counter == int(video_fps * 5):
            frame_counter = 0
            time_counter += 5
            xar.append(time_counter)
            yar.append(count_local)
            count_local = 0
        ax.clear()
        ax.set_ylim(bottom=0, top=50)
        ax.set_xlim(left=0, right=300)

        ax.set_title('Tổng số dừa: %d' % count_vehicle['coconut'])
        ax.set_xlabel('Thời gian (s)')
        ax.set_ylabel('Số lượng dừa (quả)')

        ax.plot(xar, yar)
        # redraw the canvas
        fig.canvas.draw()

        # convert canvas to image
        img = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
        img = img.reshape(fig.canvas.get_width_height()[::-1] + (3,))
        # img is rgb, convert to opencv's default bgr
        img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
        img = cv2.resize(img, (fixed_size[1], fixed_size[1]))
        # cv2.imshow("plot", img)

        # Display the resulting tracking frame
        x = 30
        y = 30
        dy = 20
        i = 0
        font = cv2.FONT_HERSHEY_COMPLEX_SMALL

        # for key, value in count_vehicle.items():
        #     text = key + ':' + str(value)
        #     cv2.putText(result, text, (x, y + dy * i), font, 1, (255, 0, 255), 2, cv2.LINE_AA)
        #     i += 1
        cv2.rectangle(result, (ix, iy), (ex, ey), (0, 255, 0), 0)

        result = np.hstack((result, img))
        cv2.imshow('Tracking', result)
        out.write(result)

        # Check for key strokes
        k = cv2.waitKey(1) & 0xff
        if k == ord('n'):
            continue
        elif k == 27:  # 'esc' key has been pressed, exit program.
            break

    # When everything done, release the capture
    out.release()
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":
    # execute main
    main()
