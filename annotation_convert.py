import os

f = open("annotation2.txt", "r")
g = open("annotations.txt", "w")

s = f.read()
s = s.split('\n')

for i in range(len(s) - 1):
    tmp = s[i].split(' ')
    str = tmp[0] + ' '
    num_obj = int(tmp[1])
    for j in range(num_obj):
        str_tmp = ''
        for k in range(4):
            str_tmp += tmp[2 + j * 4 + k] + ','
        str_tmp += '0 '
        str += str_tmp
    str += '\n'

    g.write(str)
    if i == 1:
        print(str)
