# -*- coding: utf-8 -*-
from PyQt4 import QtCore, QtGui, Qt, uic
from detectors import YOLO
import cv2
import sys
from PIL import Image
from tracker_demo import Tracker
import numpy as np
import math
import matplotlib.pyplot as plt
import threading
import queue
import colorsys
import matplotlib

try:
    from PyQt4.QtCore import QString
except ImportError:
    QString = str
matplotlib.rc('font', **{'sans-serif' : 'Arial',
                         'family' : 'sans-serif'})

ix, iy, ex, ey = -1, -1, -1, -1
frame_sz = (1280, 720)
cap_from_stream = True
path = 'coconut1.mp4'

form_class = uic.loadUiType("coconut_vi.ui")[0]
running = False
capture_thread = None
q = queue.Queue()

isSpeed = False
isOverlay = False
isName = False
isConfid = False
isTracker = False


def bb_intersection_over_union(boxA, boxB):
    # determine the (x, y)-coordinates of the intersection rectangle
    boxA = np.array(boxA).reshape((4,))
    boxB = np.array(boxB).reshape((4,))
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = max(0, xB - xA + 1) * max(0, yB - yA + 1)

    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = (boxA[2] - boxA[0] + 1) * (boxA[3] - boxA[1] + 1)
    boxBArea = (boxB[2] - boxB[0] + 1) * (boxB[3] - boxB[1] + 1)

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    # return the intersection over union value
    return iou


def draw_rec(event, x, y, flags, param):
    global ix, iy, ex, ey, drawing, mode

    if event == cv2.EVENT_LBUTTONDOWN:
        ix, iy = x, y

    elif event == cv2.EVENT_LBUTTONUP:
        ex, ey = x, y
        cv2.rectangle(param, (ix, iy), (x, y), (0, 255, 0), 0)


def get_crop_size(video_path):
    cap = cv2.VideoCapture(video_path)
    while cap.isOpened():
        ret, frame = cap.read()
        if cap_from_stream:
            frame = cv2.resize(frame, frame_sz)
        cv2.namedWindow('draw_rectangle')
        cv2.setMouseCallback('draw_rectangle', draw_rec, frame)
        print("Choose your area of interest!")
        while 1:
            cv2.imshow('draw_rectangle', frame)
            k = cv2.waitKey(1) & 0xFF
            if k == ord('a'):
                cv2.destroyAllWindows()
                break
        break


# Create opencv video capture object
cap = cv2.VideoCapture(path)
w = int(cap.get(3))
h = int(cap.get(4))
if cap_from_stream:
    w = frame_sz[0]
    h = frame_sz[1]

video_fps = cap.get(cv2.CAP_PROP_FPS)
meter_per_pixel = 0.01

# Create Object Detector
detector = YOLO()

# Create Object Tracker
tracker = Tracker(iou_thresh=0.3, max_frames_to_skip=5, max_trace_length=20, trackIdCount=0)

# Variables initialization
track_colors = [(255, 0, 0), (0, 255, 0), (0, 0, 255), (255, 255, 0),
                (0, 255, 255), (255, 0, 255), (255, 127, 255),
                (127, 0, 255), (127, 0, 127)]

count_vehicle = {'coconut': 0}

# Variables to plot
xar = []
yar = {'coconut': []}
count_local = {'coconut': 0}
fig = plt.figure(figsize=(6, 6))
ax = fig.add_subplot(1, 1, 1)

get_crop_size(path)
print('Your area of interest: ', ix, ' ', iy, ' ', ex, ' ', ey)
area = (ix, iy, ex, ey)
S_ROI = (ex - ix + 1) * (ey - iy + 1)

mapped_label = {'coconut': 70}

hsv_tuples = [(x / 80, 1., 1.)
              for x in range(80)]
colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
colors = list(
    map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)), colors))

font = cv2.FONT_HERSHEY_COMPLEX_SMALL


def grab(cam, queue, width, height):
    global running

    capture = cv2.VideoCapture(cam)
    capture.set(cv2.CAP_PROP_FRAME_WIDTH, width)
    capture.set(cv2.CAP_PROP_FRAME_HEIGHT, height)

    if type(cam) == int:
        while running:
            while 1:
                if queue.qsize() <= 1000:
                    break
            fr = {}
            capture.grab()
            retval, img = capture.retrieve(0)
            if not retval:
                continue
            fr["img"] = img
            if queue.qsize() < 2:
                queue.put(fr)
    else:
        while running:

            fr = {}
            #capture.grab()
            retval, img = capture.read()
            if not retval:
                continue
            fr["img"] = img
            queue.put(fr)


class OwnImageWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(OwnImageWidget, self).__init__(parent)
        self.image = None

    def setImage(self, image):
        self.image = image
        sz = image.size()
        self.setMinimumSize(sz)
        self.update()

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        if self.image:
            qp.drawImage(QtCore.QPoint(0, 0), self.image)
        qp.end()


class MyWindowClass(QtGui.QMainWindow, form_class):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)
        self.setupUi(self)

        self.startButton.clicked.connect(self.start_clicked)

        self.cbSpeed.stateChanged.connect(self.speed_change)
        self.cbOverlay.stateChanged.connect(self.overlay_change)
        self.cbName.stateChanged.connect(self.name_change)
        self.cbConfid.stateChanged.connect(self.confid_change)
        self.cbTracker.stateChanged.connect(self.tracker_change)

        self.camera_width = self.ImgWidget.frameSize().width()
        self.camera_height = self.ImgWidget.frameSize().height()
        self.ImgWidget = OwnImageWidget(self.ImgWidget)
        self.graph_width = self.GraphWidget.frameSize().width()
        self.graph_height = self.GraphWidget.frameSize().height()
        self.GraphWidget = OwnImageWidget(self.GraphWidget)

        self.timer = QtCore.QTimer(self)
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(1)

        self.frame_counter = 0
        self.time_counter = 0
        self.total_frame = 0

    def start_clicked(self):
        global capture_thread, running
        capture_thread = threading.Thread(target=grab, args=(path, q, 640, 360))
        running = True
        capture_thread.start()
        self.startButton.setEnabled(False)
        self.startButton.setText('Starting...')

    def update_frame(self):
        if not q.empty():
            self.startButton.setText('Tracking...')
            fr = q.get()
            frame = fr["img"]

            # --------
            if cap_from_stream:
                frame = cv2.resize(frame, frame_sz)
            frame = Image.fromarray(frame)
            # Detect and return centeroids of the objects in the frame
            result, centers, box_detected, obj_type, conf_score = detector.detect_image(frame, area)
            result = np.asarray(result)

            self.frame_counter += 1
            self.total_frame += 1
            print('Number of detections: ', len(centers))

            # Calculate density of vehicle
            S_total = 0
            for i in range(len(box_detected)):
                x, y, u, v = box_detected[i]
                S_total += (u - x + 1) * (v - y + 1)
            for i in range(len(box_detected) - 1):
                for j in range(i + 1, len(box_detected)):
                    S_total -= bb_intersection_over_union(box_detected[i], box_detected[j])
            density = S_total * 1.0 / S_ROI

            if isOverlay:
                output = result.copy()
                alpha = 0.3
                for i in range(len(box_detected)):
                    left, top, right, bottom = box_detected[i][0], box_detected[i][1], box_detected[i][2], \
                                               box_detected[i][3]
                    cv2.rectangle(result, (left, top), (right, bottom), (0, 255, 0), -1)
                    cv2.addWeighted(result, alpha, output, 1 - alpha, 0, output)

                result = output.copy()

            velocity = {'coconut': []}
            # If centroids are detected then track them
            if len(box_detected) > 0:

                # Track object using Kalman Filter
                tracker.Update(box_detected, obj_type, self.total_frame, conf_score)

                # For identified object tracks draw tracking line
                # Use various colors to indicate different track_id
                for i in range(len(tracker.tracks)):
                    if len(tracker.tracks[i].trace) >= 5:
                        for j in range(len(tracker.tracks[i].trace) - 1):
                            # Draw trace line
                            x1 = tracker.tracks[i].trace[j][0][0]
                            y1 = tracker.tracks[i].trace[j][1][0]
                            x2 = tracker.tracks[i].trace[j + 1][0][0]
                            y2 = tracker.tracks[i].trace[j + 1][1][0]
                            clr = tracker.tracks[i].track_id % 9
                            if isTracker:
                                cv2.line(result, (int(x1), int(y1)), (int(x2), int(y2)),
                                         track_colors[clr], 2)

                        classes = tracker.tracks[i].get_obj()

                        # Counting vehicle
                        if (len(tracker.tracks[i].trace) >= 10) and (not tracker.tracks[i].counted):
                            bbox = tracker.tracks[i].ground_truth_box.reshape((4, 1))
                            tracker.tracks[i].label = classes
                            tracker.tracks[i].counted = True
                            count_vehicle[classes] += 1
                            count_local[classes] += 1
                            cv2.rectangle(result, (bbox[0][0], bbox[1][0]), (bbox[2][0], bbox[3][0]),
                                          color=(255, 0, 255),
                                          thickness=3)
                    # Calculate velocity
                    if (len(tracker.tracks[i].trace) >= 10) and (len(tracker.tracks[i].box) >= 2):
                        avg = []
                        for j in range(len(tracker.tracks[i].box) - 1):
                            x1 = tracker.tracks[i].box[j][0][0]
                            y1 = tracker.tracks[i].box[j][0][1]
                            f1 = tracker.tracks[i].box[j][1]

                            x2 = tracker.tracks[i].box[j + 1][0][0]
                            y2 = tracker.tracks[i].box[j + 1][0][1]
                            f2 = tracker.tracks[i].box[j + 1][1]

                            d = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2) / (f2 - f1)
                            v1 = d * (video_fps - 1) * meter_per_pixel  # m/s
                            v2 = v1 * 3.6  # km/h
                            avg.append(v2)

                        avg = np.mean(avg)
                        velocity[tracker.tracks[i].label].append(avg)

                        # Drawing in frame
                        if tracker.tracks[i].has_truebox:
                            bbox = tracker.tracks[i].ground_truth_box.reshape((4, 1))
                            left, top, right, bottom = bbox[0][0], bbox[1][0], bbox[2][0], bbox[3][0]
                            predicted_class = tracker.tracks[i].label
                            score = tracker.tracks[i].conf_score[-1]
                            v = avg

                            if isSpeed or isName or isConfid:
                                speed_str = '{:.2f} km/h'.format(v)
                                name_str = '{}'.format(predicted_class)
                                conf_str = '{:.2f}p'.format(score)

                                label = ''
                                if isName:
                                    label += name_str
                                if isConfid:
                                    if label != '':
                                        label += ' '
                                    label += conf_str
                                if isSpeed:
                                    if label != '':
                                        label += ' '
                                    label += speed_str

                                ret, baseline = cv2.getTextSize(label, fontFace=font, fontScale=0.8, thickness=1)
                                cv2.rectangle(result, (left, top), (left + ret[0], top + ret[1] + baseline),
                                              color=colors[mapped_label[predicted_class]], thickness=-1)
                                cv2.putText(result, label, (left, top + ret[1] + baseline), font, 0.8, (0, 0, 0), 1,
                                            cv2.LINE_AA)

                            # if isOverlay:
                            #     output = result.copy()
                            #     alpha = 0.3
                            #     cv2.rectangle(result, (left, top), (right, bottom), (0, 255, 0), -1)
                            #     cv2.addWeighted(result, alpha, output, 1 - alpha, 0, output)
                            #
                            #     result = output.copy()
                                # cv2.rectangle(result, (bbox[0][0], bbox[1][0]), (bbox[2][0], bbox[3][0]),
                                #               color=(255, 0, 255),
                                #               thickness=3)

            # Plot graph
            if self.frame_counter == int(video_fps * 3):
                self.frame_counter = 0
                self.time_counter += 3
                xar.append(self.time_counter)
                for key, value in count_local.items():
                    yar[key].append(value)
                    count_local[key] = 0
            ax.clear()
            ax.set_ylim(bottom=0, top=50)
            ax.set_xlim(left=0, right=300)
            ax.set_xlabel('Thời gian (s)')
            ax.set_ylabel('Số lượng dừa')
            # ax.set_title('')
            for key, value in yar.items():
                ax.plot(xar, value, label='Dừa')
            handles, labels = ax.get_legend_handles_labels()
            ax.legend(handles, labels)
            fig.canvas.draw()
            img = np.fromstring(fig.canvas.tostring_rgb(), dtype=np.uint8, sep='')
            img = img.reshape(fig.canvas.get_width_height()[::-1] + (3,))
            img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
            img = cv2.resize(img, (640, 640))

            # Display velocity
            for key, value in velocity.items():
                v = np.mean(value)
                if np.isnan(v):
                    v = 0
                velocity[key] = v

            cv2.rectangle(result, (ix, iy), (ex, ey), (0, 255, 0), 0)
            frame = result.copy()
            graph_img = img
            # 'frame': input image variable from camera/video:

            graph_img = cv2.cvtColor(graph_img, cv2.COLOR_RGB2BGR)

            # --------
            # update density
            self.denLabel.setText(QString('%d' % (count_vehicle['coconut'])))


            # update image 'frame' to ImgWidget
            img_height, img_width, img_colors = frame.shape
            scale_w = float(self.camera_width) / float(img_width)
            scale_h = float(self.camera_height) / float(img_height)
            scale = min([scale_w, scale_h])
            if scale == 0:
                scale = 1
            frame = cv2.resize(frame, None, fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            height, width, bpc = frame.shape
            bpl = bpc * width
            image = QtGui.QImage(frame.data, width, height, bpl, QtGui.QImage.Format_RGB888)
            self.ImgWidget.setImage(image)

            # update image 'graph_img' to GraphWidget
            img_height, img_width, img_colors = graph_img.shape
            scale_w = float(self.graph_width) / float(img_width)
            scale_h = float(self.graph_height) / float(img_height)
            scale = min([scale_w, scale_h])
            if scale == 0:
                scale = 1
            graph_img = cv2.resize(graph_img, None, fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)
            graph_img = cv2.cvtColor(graph_img, cv2.COLOR_BGR2RGB)
            height, width, bpc = graph_img.shape
            bpl = bpc * width
            image = QtGui.QImage(graph_img.data, width, height, bpl, QtGui.QImage.Format_RGB888)
            self.GraphWidget.setImage(image)

    def speed_change(self, state):
        global isSpeed
        if state == QtCore.Qt.Checked:
            isSpeed = True
        else:
            isSpeed = False

    def overlay_change(self, state):
        global isOverlay
        if state == QtCore.Qt.Checked:
            isOverlay = True
        else:
            isOverlay = False

    def name_change(self, state):
        global isName
        if state == QtCore.Qt.Checked:
            isName = True
        else:
            isName = False

    def confid_change(self, state):
        global isConfid
        if state == QtCore.Qt.Checked:
            isConfid = True
        else:
            isConfid = False

    def tracker_change(self, state):
        global isTracker
        if state == QtCore.Qt.Checked:
            isTracker = True
        else:
            isTracker = False

    def closeEvent(self, event):
        global running
        running = False


def main():
    app = QtGui.QApplication(sys.argv)
    w = MyWindowClass(None)
    w.setWindowTitle('Vehicle Tracking')
    w.show()
    app.exec_()


if __name__ == "__main__":
    # execute main
    main()
